# Translation of kategdbplugin.pot into esperanto.
# Copyright (C) 2011 Free Software Foundation, Inc.
# This file is distributed under the same license as the kate package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 01:00+0000\n"
"PO-Revision-Date: 2023-03-11 03:39+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "GDB-komando"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Fontdosier-serĉvojoj"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Loka aplikaĵo"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Fora TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Fora Seria Konektejo"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Gastiganto"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Konektejo"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absoluta-prefikso"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Propraj Init-Komandoj"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Sencimiga seanco estas en kurso. Bonvolu uzi re-run aŭ ĉesigi la nunan "
"seancon."

#: configview.cpp:91
#, kde-format
msgid "Add new target"
msgstr "Aldoni novan celon"

#: configview.cpp:95
#, kde-format
msgid "Copy target"
msgstr "Kopii celon"

#: configview.cpp:99
#, kde-format
msgid "Delete target"
msgstr "Forigi celon"

#: configview.cpp:104
#, kde-format
msgid "Executable:"
msgstr "Efektivigebla:"

#: configview.cpp:124
#, kde-format
msgid "Working Directory:"
msgstr "Labora Dosierujo:"

#: configview.cpp:132
#, kde-format
msgid "Process Id:"
msgstr "Proceza Id:"

#: configview.cpp:137
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumentoj:"

#: configview.cpp:140
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Konservi fokuson"

#: configview.cpp:141
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Konservi la fokuson sur la komandlinio"

#: configview.cpp:143
#, kde-format
msgid "Redirect IO"
msgstr "Alidirekti IO"

#: configview.cpp:144
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Redirektas la sencimigitajn programojn IO al aparta langeto"

#: configview.cpp:146
#, kde-format
msgid "Advanced Settings"
msgstr "Altnivelaj Agordoj"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Celoj"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Celo %1"

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Lokuloj"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "CPU-registroj"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""
"Bonvolu agordi la plenumeblan en la langeto 'Agordoj' en la panelo "
"'Sencimigi'."

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Neniu erarserĉilo elektita. Bonvolu elekti unu en la langeto 'Agordoj' en la "
"panelo 'Sencimigi'."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Sencimigilo ne trovita. Bonvolu certigi, ke vi havas ĝin instalita en via "
"sistemo. La elektita erarserĉilo estas '%1'"

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Ne eblis komenci erarserĉilprocezon"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb eliris normale ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "ĉiuj fadenoj kurantaj"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "fadeno(j) kuranta(j): %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "haltis (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Aktiva fadeno: %1 (ĉiuj fadenoj ĉesis)."

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Aktiva fadeno: %1."

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Nuna kadro: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Gastiganto: %1. Celo: %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: Ne eblis analizi lastan respondon: %1. Tro da sinsekvaj eraroj. "
"Fermante."

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: Ne eblis analizi lastan respondon: %1"

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "DAP-backend malsukcesis"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "programo finiĝis"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "petante malkonekton"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "petante ĉesigon"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "DAP backend: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Paŭzopunkto(j) atingita(j):"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(daŭrigo) fadeno %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "ĉiuj fadenoj daŭris"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(kurante)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** konekto kun servilo fermita ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "programo eliris kun kodo %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** atendante uzantajn agojn ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "eraro pri respondo: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "grava"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetrio"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "sencimiga procezo [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "sencimiga procezo %1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Komenca metodo: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "fadeno %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "paŭzopunkto aro"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "paŭzopunkto malbarita"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) paŭzopunkto"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<ne taksita>"

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "servilaj kapabloj"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "apogita"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "nesubtenata"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "kondiĉaj paŭzopunktoj"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "funkciaj paŭzopunktoj"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "trafi kondiĉajn paŭzopunktojn"

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "logpunktoj"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "peto de moduloj"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "iri al celpeto"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "ĉesigi peton"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "fini sencimigi"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "sintaksa eraro: esprimo ne trovita"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "sintaksa eraro: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "nevalida linio: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "dosiero ne specifita: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "nevalida fadenidigilo: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "fadenidentigilo ne specifita: %1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Disponeblaj komandoj:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "kondiĉaj paŭzopunktoj ne estas subtenataj de la servilo"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr "trafaj kondiĉaj paŭzopunktoj ne estas subtenataj de la servilo"

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "linio %1 jam havas paŭzopunkton"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "paŭzopunkto ne trovita (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Nuna fadeno:"

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "neniu"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Nuna kadro:"

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Seanca stato:"

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "komencante"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "kurante"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "haltis"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "finita"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "malkonektita"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "post mortem"

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "komando ne trovita"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "mankas fadenid"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "mortiganta backend"

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Nuna kadro [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Simbolo"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Valoro"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "tipo"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "indeksitaj eroj"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "nomitaj eroj"

#: plugin_kategdb.cpp:66
#, kde-format
msgid "Kate GDB"
msgstr "Kate GDB"

#: plugin_kategdb.cpp:70
#, kde-format
msgid "Debug View"
msgstr "Sencimiga Vido"

#: plugin_kategdb.cpp:70 plugin_kategdb.cpp:285
#, kde-format
msgid "Debug"
msgstr "Sencimigi"

#: plugin_kategdb.cpp:73 plugin_kategdb.cpp:76
#, kde-format
msgid "Locals and Stack"
msgstr "Lokuloj kaj Stako"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nr"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Kadro"

#: plugin_kategdb.cpp:153
#, kde-format
msgctxt "Tab label"
msgid "GDB Output"
msgstr "GDB Eligo"

#: plugin_kategdb.cpp:154
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Agordoj"

#: plugin_kategdb.cpp:196
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Ne eblis malfermi dosieron:</title><nl/>%1<br/>Provu aldoni serĉvojon "
"al Altnivelaj Agordoj -> Fontdosierserĉaj vojoj"

#: plugin_kategdb.cpp:221
#, kde-format
msgid "Start Debugging"
msgstr "Komenci Sencimigon"

#: plugin_kategdb.cpp:227
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Mortigi / Ĉesi Sencimigon"

#: plugin_kategdb.cpp:233
#, kde-format
msgid "Restart Debugging"
msgstr "Rekomenci Sencimigon"

#: plugin_kategdb.cpp:239
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Baskuligi Paŭzpunkton/Paŭzon"

#: plugin_kategdb.cpp:245
#, kde-format
msgid "Step In"
msgstr "Enpaŝi"

#: plugin_kategdb.cpp:251
#, kde-format
msgid "Step Over"
msgstr "Paŝi super"

#: plugin_kategdb.cpp:257
#, kde-format
msgid "Step Out"
msgstr "Elpaŝi"

#: plugin_kategdb.cpp:263 plugin_kategdb.cpp:294
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Movi programindekson (PC)"

#: plugin_kategdb.cpp:268 plugin_kategdb.cpp:292
#, kde-format
msgid "Run To Cursor"
msgstr "Ruli Al Kursoro"

#: plugin_kategdb.cpp:274
#, kde-format
msgid "Continue"
msgstr "Daŭrigi"

#: plugin_kategdb.cpp:280
#, kde-format
msgid "Print Value"
msgstr "Printi Valoron"

#: plugin_kategdb.cpp:289
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:373 plugin_kategdb.cpp:389
#, kde-format
msgid "Insert breakpoint"
msgstr "Enigi paŭzopunkton"

#: plugin_kategdb.cpp:387
#, kde-format
msgid "Remove breakpoint"
msgstr "Forigi paŭzopunkton"

#: plugin_kategdb.cpp:519
#, kde-format
msgid "Execution point"
msgstr "Programrula punkto"

#: plugin_kategdb.cpp:662
#, kde-format
msgid "Thread %1"
msgstr "Fadeno %1"

#: plugin_kategdb.cpp:762
#, kde-format
msgid "IO"
msgstr "IO"

#: plugin_kategdb.cpp:848
#, kde-format
msgid "Breakpoint"
msgstr "Paŭzopunkto"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Sencimigi"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "GDB Plugin"
msgstr "GDB-Kromprogramo"
