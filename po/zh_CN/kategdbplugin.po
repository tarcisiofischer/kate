msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 01:00+0000\n"
"PO-Revision-Date: 2023-03-11 04:52\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kate/kategdbplugin.pot\n"
"X-Crowdin-File-ID: 5316\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "GDB 命令"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "源文件搜索路径"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "本地程序"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "远程 TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "删除串口"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "主机"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "端口"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "波特率"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "自定义初始化命令"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr "有一个调试会话正在运行。请重新运行或停止当前会话。"

#: configview.cpp:91
#, kde-format
msgid "Add new target"
msgstr "添加新目标"

#: configview.cpp:95
#, kde-format
msgid "Copy target"
msgstr "复制目标"

#: configview.cpp:99
#, kde-format
msgid "Delete target"
msgstr "删除目标"

#: configview.cpp:104
#, kde-format
msgid "Executable:"
msgstr "可执行文件"

#: configview.cpp:124
#, kde-format
msgid "Working Directory:"
msgstr "工作目录："

#: configview.cpp:132
#, kde-format
msgid "Process Id:"
msgstr "进程 ID："

#: configview.cpp:137
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "参数："

#: configview.cpp:140
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "保持焦点"

#: configview.cpp:141
#, kde-format
msgid "Keep the focus on the command line"
msgstr "焦点保持在命令行上"

#: configview.cpp:143
#, kde-format
msgid "Redirect IO"
msgstr "重定向 IO"

#: configview.cpp:144
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "将被调试程序的输入输出重定向到另外标签页"

#: configview.cpp:146
#, kde-format
msgid "Advanced Settings"
msgstr "高级设置"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "目标"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "目标 %1"

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "局部变量"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "CPU 寄存器"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "无法启动调试器进程"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb 正常退出 ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "所有线程正在运行"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "正在运行的线程：%1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "停止 (%1)"

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "活动线程：%1 (所有线程已停止)"

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "活动线程：%1"

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "当前帧：%1：%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "主机：%1 目标：%1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr "gdb-mi：无法解析上次响应：%1。已经有太多连续错误。终止。"

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi：无法解析上次响应：%1"

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "DAP 后端失败"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "程序终止"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "请求断开连接"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "请求关闭"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "DAP 后端：%1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "到达断点："

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(继续执行) 线程 %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "所有线程继续运行"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(运行中)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** 与服务器的连接已关闭 ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "程序已退出，退出码：%1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** 等待用户操作 ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "响应出错：%1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "重要"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "监视"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "调试进程：[%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "调试进程：%1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "启动方式：%1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "线程 %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "断点集合"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "断点已清除"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) 断点"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr ""

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "服务器功能"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "支持"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "不支持"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "条件断点"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "函数断点"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "命中条件断点"

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "日志点"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "模块请求"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "转到目标请求"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "终止请求"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "终止调试"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "语法错误：找不到表达式"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "语法错误：%1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "无效的行：%1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "文件未指定：%1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "无效的线程 ID: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "线程 ID 未指定：%1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "可用的命令："

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "服务器不支持条件断点"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr "服务器不支持命中条件断点"

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr ""

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "找不到断点 (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "当前线程："

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr ""

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "当前帧："

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "会话状态："

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "正在初始化"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "正在运行"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "已停止"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "已终止"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr ""

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr ""

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "找不到命令"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "缺少线程 ID"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr ""

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr ""

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "符号"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "值"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr ""

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr ""

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr ""

#: plugin_kategdb.cpp:66
#, kde-format
msgid "Kate GDB"
msgstr "Kate GDB"

#: plugin_kategdb.cpp:70
#, kde-format
msgid "Debug View"
msgstr "调试视图"

#: plugin_kategdb.cpp:70 plugin_kategdb.cpp:285
#, kde-format
msgid "Debug"
msgstr "调试"

#: plugin_kategdb.cpp:73 plugin_kategdb.cpp:76
#, kde-format
msgid "Locals and Stack"
msgstr "局部变量和堆栈"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "编号"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "框架"

#: plugin_kategdb.cpp:153
#, kde-format
msgctxt "Tab label"
msgid "GDB Output"
msgstr "GDB 输出"

#: plugin_kategdb.cpp:154
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "设置"

#: plugin_kategdb.cpp:196
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>无法打开文件：</title><nl/>%1<br/>尝试添加搜索路径到高级设置 -> 源文件"
"搜索路径"

#: plugin_kategdb.cpp:221
#, kde-format
msgid "Start Debugging"
msgstr "启动调试"

#: plugin_kategdb.cpp:227
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "杀死/停止调试"

#: plugin_kategdb.cpp:233
#, kde-format
msgid "Restart Debugging"
msgstr "重启调试"

#: plugin_kategdb.cpp:239
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "切换断点"

#: plugin_kategdb.cpp:245
#, kde-format
msgid "Step In"
msgstr "单步跳入"

#: plugin_kategdb.cpp:251
#, kde-format
msgid "Step Over"
msgstr "单步跳过"

#: plugin_kategdb.cpp:257
#, kde-format
msgid "Step Out"
msgstr "单步跳出"

#: plugin_kategdb.cpp:263 plugin_kategdb.cpp:294
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "下条指令"

#: plugin_kategdb.cpp:268 plugin_kategdb.cpp:292
#, kde-format
msgid "Run To Cursor"
msgstr "运行到光标处"

#: plugin_kategdb.cpp:274
#, kde-format
msgid "Continue"
msgstr "继续"

#: plugin_kategdb.cpp:280
#, kde-format
msgid "Print Value"
msgstr "打印值"

#: plugin_kategdb.cpp:289
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:373 plugin_kategdb.cpp:389
#, kde-format
msgid "Insert breakpoint"
msgstr "插入断点"

#: plugin_kategdb.cpp:387
#, kde-format
msgid "Remove breakpoint"
msgstr "删除断点"

#: plugin_kategdb.cpp:519
#, kde-format
msgid "Execution point"
msgstr "执行点"

#: plugin_kategdb.cpp:662
#, kde-format
msgid "Thread %1"
msgstr "线程 %1"

#: plugin_kategdb.cpp:762
#, kde-format
msgid "IO"
msgstr "输入输出"

#: plugin_kategdb.cpp:848
#, kde-format
msgid "Breakpoint"
msgstr "断点"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "调试(&D)"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "GDB Plugin"
msgstr "GDB 插件"
