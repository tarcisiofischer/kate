# translation of kateproject.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2012, 2013, 2014, 2015, 2016, 2019.
# Mthw <jari_45@hotmail.com>, 2018, 2019.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2020, 2021, 2022, 2023.
# Ferdinand Galko <galko.ferdinand@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kateproject\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-14 00:49+0000\n"
"PO-Revision-Date: 2023-01-31 18:13+0100\n"
"Last-Translator: Ferdinand Galko <galko.ferdinand@gmail.com>\n"
"Language-Team: Slovak <opensuse-translation@opensuse.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: branchcheckoutdialog.cpp:29
#, kde-format
msgid "Select branch to checkout. Press 'Esc' to cancel."
msgstr "Vyberte vetvu na získanie. Stlačením 'Esc' zrušíte výber."

#: branchcheckoutdialog.cpp:36
#, kde-format
msgid "Create New Branch"
msgstr "Vytvoriť novú vetvu"

#: branchcheckoutdialog.cpp:38
#, kde-format
msgid "Create New Branch From..."
msgstr "Vytvoriť novú vetvu z..."

#: branchcheckoutdialog.cpp:53
#, kde-format
msgid "Branch %1 checked out"
msgstr "Vetva %1 sa získala"

#: branchcheckoutdialog.cpp:56
#, kde-format
msgid "Failed to checkout to branch %1, Error: %2"
msgstr "Nepodarilo sa prejsť na vetvu %1, Chyba: %2"

#: branchcheckoutdialog.cpp:83 branchcheckoutdialog.cpp:95
#, kde-format
msgid "Enter new branch name. Press 'Esc' to cancel."
msgstr "Zadajte názov novej vetvy. Stlačením 'Esc' zrušíte zadávanie."

#: branchcheckoutdialog.cpp:100
#, kde-format
msgid "Select branch to checkout from. Press 'Esc' to cancel."
msgstr ""
"Vyberte vetvu, z ktorej sa má uskutočniť získanie. Stlačením 'Esc' zrušíte "
"výber."

#: branchcheckoutdialog.cpp:121
#, kde-format
msgid "Checked out to new branch: %1"
msgstr "Prešlo sa na novú vetvu: %1"

#: branchcheckoutdialog.cpp:123
#, kde-format
msgid "Failed to create new branch. Error \"%1\""
msgstr "Nepodarilo sa vytvoriť novú vetvu. Chyba \"%1\""

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Branch"
msgstr "Vetva"

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Last Commit"
msgstr "Posledné začlenenie"

#: branchdeletedialog.cpp:135 kateprojecttreeviewcontextmenu.cpp:78
#, kde-format
msgid "Delete"
msgstr "Vymazať"

#: branchdeletedialog.cpp:140
#, kde-format
msgid "Are you sure you want to delete the selected branch?"
msgid_plural "Are you sure you want to delete the selected branches?"
msgstr[0] "Naozaj chcete vymazať vybranú vetvu?"
msgstr[1] "Naozaj chcete vymazať vybrané vetvy?"
msgstr[2] "Naozaj chcete vymazať vybrané vetvy?"

#: branchesdialog.cpp:102
#, kde-format
msgid "Select Branch..."
msgstr "Vybrať vetvu..."

#: branchesdialog.cpp:128 gitwidget.cpp:490 kateprojectpluginview.cpp:68
#, kde-format
msgid "Git"
msgstr "Git"

#: comparebranchesview.cpp:144
#, kde-format
msgid "Back"
msgstr "Späť"

#: currentgitbranchbutton.cpp:123
#, kde-format
msgctxt "Tooltip text, describing that '%1' commit is checked out"
msgid "HEAD at commit %1"
msgstr "HEAD na začlenení %1"

#: currentgitbranchbutton.cpp:125
#, kde-format
msgctxt "Tooltip text, describing that '%1' tag is checked out"
msgid "HEAD is at this tag %1"
msgstr "HEAD na tejto značke %1"

#: currentgitbranchbutton.cpp:127
#, kde-format
msgctxt "Tooltip text, describing that '%1' branch is checked out"
msgid "Active branch: %1"
msgstr "Aktívna vetva: %1"

#: gitcommitdialog.cpp:69 gitcommitdialog.cpp:116
#, kde-format
msgid "Commit Changes"
msgstr "Začleniť zmeny"

#: gitcommitdialog.cpp:73 gitcommitdialog.cpp:115 gitwidget.cpp:251
#, kde-format
msgid "Commit"
msgstr "Začleniť"

#: gitcommitdialog.cpp:74
#, kde-format
msgid "Cancel"
msgstr "Zrušiť"

#: gitcommitdialog.cpp:76
#, kde-format
msgid "Write commit message..."
msgstr "Napísať správu pre začlenenie..."

#: gitcommitdialog.cpp:83
#, kde-format
msgid "Extended commit description..."
msgstr "Rozšírený popis začlenenia..."

#: gitcommitdialog.cpp:107
#, kde-format
msgid "Sign off"
msgstr "Odhlásiť sa"

#: gitcommitdialog.cpp:111 gitcommitdialog.cpp:120
#, kde-format
msgid "Amend"
msgstr "Doplniť"

#: gitcommitdialog.cpp:112 gitwidget.cpp:961
#, kde-format
msgid "Amend Last Commit"
msgstr "Doplniť posledné začlenenie"

#: gitcommitdialog.cpp:119
#, kde-format
msgid "Amending Commit"
msgstr "Doplnenie začlenenia"

#: gitcommitdialog.cpp:203
#, kde-format
msgctxt "Number of characters"
msgid "%1 / 52"
msgstr "%1 / 52"

#: gitcommitdialog.cpp:207
#, kde-format
msgctxt "Number of characters"
msgid "<span style=\"color:%1;\">%2</span> / 52"
msgstr "<span style=\"color:%1;\">%2</span> / 52"

#: gitstatusmodel.cpp:85
#, kde-format
msgid "Staged"
msgstr "Označené"

#: gitstatusmodel.cpp:87
#, kde-format
msgid "Untracked"
msgstr "Nesledované"

#: gitstatusmodel.cpp:89
#, kde-format
msgid "Conflict"
msgstr "Konflikt"

#: gitstatusmodel.cpp:91
#, kde-format
msgid "Modified"
msgstr "Zmenené"

#: gitwidget.cpp:259
#, kde-format
msgid "Git Push"
msgstr "Git Push"

#: gitwidget.cpp:272
#, kde-format
msgid "Git Pull"
msgstr "Git Pull"

#: gitwidget.cpp:285
#, kde-format
msgid "Cancel Operation"
msgstr "Zrušiť operáciu"

#: gitwidget.cpp:293
#, kde-format
msgid " canceled."
msgstr " zrušené."

#: gitwidget.cpp:314 kateprojectview.cpp:59
#, kde-format
msgid "Filter..."
msgstr "Filtrovať..."

#: gitwidget.cpp:401
#, kde-format
msgid "Failed to find .git directory for '%1', things may not work correctly"
msgstr ""
"Nepodarilo sa nájsť adresár .git pre '%1', veci nemusia fungovať správne"

#: gitwidget.cpp:575
#, kde-format
msgid " error: %1"
msgstr " chyba: %1"

#: gitwidget.cpp:581
#, kde-format
msgid "\"%1\" executed successfully: %2"
msgstr "\"%1\" úspešne vykonané: %2"

#: gitwidget.cpp:601
#, kde-format
msgid "Failed to stage file. Error:"
msgstr "Nepodarilo sa označiť súbor. Chyba:"

#: gitwidget.cpp:614
#, kde-format
msgid "Failed to unstage file. Error:"
msgstr "Nepodarilo sa zrušiť označenie súboru. Chyba:"

#: gitwidget.cpp:625
#, kde-format
msgid "Failed to discard changes. Error:"
msgstr "Nepodarilo sa zahodiť zmeny. Chyba:"

#: gitwidget.cpp:636
#, kde-format
msgid "Failed to remove. Error:"
msgstr "Nepodarilo sa odstrániť. Chyba:"

#: gitwidget.cpp:652
#, kde-format
msgid "Failed to open file at HEAD: %1"
msgstr "Nepodarilo sa otvoriť súbor na HEAD: %1"

#: gitwidget.cpp:684
#, kde-format
msgid "Failed to get Diff of file: %1"
msgstr "Nepodarilo sa získať Diff súboru: %1"

#: gitwidget.cpp:751
#, kde-format
msgid "Failed to commit: %1"
msgstr "Nepodarilo sa začleniť: %1"

#: gitwidget.cpp:755
#, kde-format
msgid "Changes committed successfully."
msgstr "Zmeny boli úspešne začlenené."

#: gitwidget.cpp:765
#, kde-format
msgid "Nothing to commit. Please stage your changes first."
msgstr "Nič na začlenenie. Prosím, najskôr označte svoje zmeny."

#: gitwidget.cpp:778
#, kde-format
msgid "Commit message cannot be empty."
msgstr "Správa začlenenia nemôže byť prázdna."

#: gitwidget.cpp:893
#, kde-format
msgid "No diff for %1...%2"
msgstr "Žiadny rozdiel pre %1...%2"

#: gitwidget.cpp:899
#, kde-format
msgid "Failed to compare %1...%2"
msgstr "Nepodarilo sa porovnať %1...%2"

#: gitwidget.cpp:914
#, kde-format
msgid "Failed to get numstat when diffing %1...%2"
msgstr "Nepodarilo sa získať numstat pri porovnávaní %1...%2"

#: gitwidget.cpp:953
#, kde-format
msgid "Refresh"
msgstr "Obnoviť"

#: gitwidget.cpp:969
#, kde-format
msgid "Checkout Branch"
msgstr "Získať vetvu"

#: gitwidget.cpp:981
#, kde-format
msgid "Delete Branch"
msgstr "Vybrať vetvu"

#: gitwidget.cpp:993
#, kde-format
msgid "Compare Branch with..."
msgstr "Porovnať vetvu s..."

#: gitwidget.cpp:997
#, kde-format
msgid "Show Commit"
msgstr "Zobraziť začlenenie"

#: gitwidget.cpp:997
#, kde-format
msgid "Commit hash"
msgstr "Hash začlenenie"

#: gitwidget.cpp:1002
#, kde-format
msgid "Open Commit..."
msgstr "Otvoriť začlenenie..."

#: gitwidget.cpp:1005 gitwidget.cpp:1044
#, kde-format
msgid "Stash"
msgstr "Úschovňa"

#: gitwidget.cpp:1015
#, kde-format
msgid "Diff - stash"
msgstr "Rozdiel - úschovňa"

#: gitwidget.cpp:1048
#, kde-format
msgid "Pop Last Stash"
msgstr "Aplikovať a vymazať poslednú úschovňu"

#: gitwidget.cpp:1052
#, kde-format
msgid "Pop Stash"
msgstr "Aplikovať a vymazať úschovňu"

#: gitwidget.cpp:1056
#, kde-format
msgid "Apply Last Stash"
msgstr "Aplikovať poslednú úschovňu"

#: gitwidget.cpp:1058
#, kde-format
msgid "Stash (Keep Staged)"
msgstr "Úschovňa (Ponechať označené)"

#: gitwidget.cpp:1062
#, kde-format
msgid "Stash (Include Untracked)"
msgstr "Úschovňa (vrátane nesledovaných)"

#: gitwidget.cpp:1066
#, kde-format
msgid "Apply Stash"
msgstr "Aplikovať úschovňu"

#: gitwidget.cpp:1067
#, kde-format
msgid "Drop Stash"
msgstr "Vymazať úschovňu"

#: gitwidget.cpp:1068
#, kde-format
msgid "Show Stash Content"
msgstr "Zobraziť obsah úschovne"

#: gitwidget.cpp:1108
#, kde-format
msgid "Stage All"
msgstr "Označiť všetko"

#: gitwidget.cpp:1110
#, kde-format
msgid "Remove All"
msgstr "Odobrať všetko"

#: gitwidget.cpp:1110
#, kde-format
msgid "Discard All"
msgstr "Zahodiť všetko"

#: gitwidget.cpp:1113
#, kde-format
msgid "Open .gitignore"
msgstr "Otvoriť .gitignore"

#: gitwidget.cpp:1114 gitwidget.cpp:1169 gitwidget.cpp:1219
#: kateprojectconfigpage.cpp:91 kateprojectconfigpage.cpp:102
#, kde-format
msgid "Show Diff"
msgstr "Zobraziť rozdiel"

#: gitwidget.cpp:1131
#, kde-format
msgid "Are you sure you want to remove these files?"
msgstr "Ste si istí, že chcete tieto súbory odstrániť?"

#: gitwidget.cpp:1140
#, kde-format
msgid "Are you sure you want to discard all changes?"
msgstr "Naozaj chcete zahodiť všetky zmeny?"

#: gitwidget.cpp:1168
#, kde-format
msgid "Open File"
msgstr "Otvoriť súbor"

#: gitwidget.cpp:1170
#, kde-format
msgid "Show in External Git Diff Tool"
msgstr "Zobraziť v externom nástroji rozdielov git"

#: gitwidget.cpp:1171
#, kde-format
msgid "Open at HEAD"
msgstr "Otvoriť na HEAD"

#: gitwidget.cpp:1172
#, kde-format
msgid "Unstage File"
msgstr "Zrušiť označenie súboru"

#: gitwidget.cpp:1172
#, kde-format
msgid "Stage File"
msgstr "Označiť súbor"

#: gitwidget.cpp:1173
#, kde-format
msgid "Remove"
msgstr "Odstrániť"

#: gitwidget.cpp:1173
#, kde-format
msgid "Discard"
msgstr "Zahodiť"

#: gitwidget.cpp:1190
#, kde-format
msgid "Are you sure you want to discard the changes in this file?"
msgstr "Naozaj chcete zahodiť zmeny v tomto súbore?"

#: gitwidget.cpp:1203
#, kde-format
msgid "Are you sure you want to remove this file?"
msgstr "Naozaj chcete odstrániť tento súbor?"

#: gitwidget.cpp:1218
#, kde-format
msgid "Unstage All"
msgstr "Zrušiť označenie všetkého"

#: gitwidget.cpp:1285
#, kde-format
msgid "Unstage Selected Files"
msgstr "Zrušiť označenie vybraných súborov"

#: gitwidget.cpp:1285
#, kde-format
msgid "Stage Selected Files"
msgstr "Označiť vybrané súbory"

#: gitwidget.cpp:1286
#, kde-format
msgid "Discard Selected Files"
msgstr "Zahodiť vybraté súbory"

#: gitwidget.cpp:1290
#, kde-format
msgid "Remove Selected Files"
msgstr "Odstrániť vybraté súbory"

#: gitwidget.cpp:1303
#, kde-format
msgid "Are you sure you want to discard the changes?"
msgstr "Naozaj chcete zahodiť zmeny?"

#: gitwidget.cpp:1312
#, kde-format
msgid "Are you sure you want to remove these untracked changes?"
msgstr "Naozaj chcete odstrániť tieto nesledované zmeny?"

#: kateproject.cpp:217
#, kde-format
msgid "Malformed JSON file '%1': %2"
msgstr "Chybne vytvorený súbor JSON '%1': %2"

#: kateproject.cpp:529
#, kde-format
msgid "<untracked>"
msgstr "<nesledované>"

#: kateprojectcompletion.cpp:43
#, kde-format
msgid "Project Completion"
msgstr "Dokončenie projektu"

#: kateprojectconfigpage.cpp:25
#, kde-format
msgctxt "Groupbox title"
msgid "Autoload Repositories"
msgstr "Automaticky načítať repozitáre"

#: kateprojectconfigpage.cpp:27
#, kde-format
msgid ""
"Project plugin is able to autoload repository working copies when there is "
"no .kateproject file defined yet."
msgstr ""
"Plugin projektu je schopný automaticky načítať pracovné kópie repozitára v "
"prípade, že je už definovaný súbor .kateproject."

#: kateprojectconfigpage.cpp:30
#, kde-format
msgid "&Git"
msgstr "&Git"

#: kateprojectconfigpage.cpp:33
#, kde-format
msgid "&Subversion"
msgstr "&Subversion"

#: kateprojectconfigpage.cpp:35
#, kde-format
msgid "&Mercurial"
msgstr "&Mercurial"

#: kateprojectconfigpage.cpp:37
#, kde-format
msgid "&Fossil"
msgstr "&Fossil"

#: kateprojectconfigpage.cpp:46
#, kde-format
msgctxt "Groupbox title"
msgid "Session Behavior"
msgstr "Správanie sedenia"

#: kateprojectconfigpage.cpp:47
#, kde-format
msgid "Session settings for projects"
msgstr "Nastavenia sedení pre projekty"

#: kateprojectconfigpage.cpp:48
#, kde-format
msgid "Restore Open Projects"
msgstr "Obnoviť otvorené projekty"

#: kateprojectconfigpage.cpp:55
#, kde-format
msgctxt "Groupbox title"
msgid "Project Index"
msgstr "Index projektu"

#: kateprojectconfigpage.cpp:56
#, kde-format
msgid "Project ctags index settings"
msgstr "Nastavenia ctag indexu projektu"

#: kateprojectconfigpage.cpp:57 kateprojectinfoviewindex.cpp:206
#, kde-format
msgid "Enable indexing"
msgstr "Povoliť indexovanie"

#: kateprojectconfigpage.cpp:60
#, kde-format
msgid "Directory for index files"
msgstr "Priečinok pre súbory indexu"

#: kateprojectconfigpage.cpp:64
#, kde-format
msgid ""
"The system temporary directory is used if not specified, which may overflow "
"for very large repositories"
msgstr ""
"Ak nie je špecifikované, používa sa systémový dočasný adresár, ktorý sa môže "
"preplniť pri veľmi veľkých archívoch"

#: kateprojectconfigpage.cpp:71
#, kde-format
msgctxt "Groupbox title"
msgid "Cross-Project Functionality"
msgstr "Medzi-projektová funkcionalita"

#: kateprojectconfigpage.cpp:72
#, kde-format
msgid ""
"Project plugin is able to perform some operations across multiple projects"
msgstr ""
"Doplnok projektu je schopný vykonávať niektoré operácie vo viacerých "
"projektoch"

#: kateprojectconfigpage.cpp:73
#, kde-format
msgid "Cross-Project Completion"
msgstr "Cross-Project Completion"

#: kateprojectconfigpage.cpp:75
#, kde-format
msgid "Cross-Project Goto Symbol"
msgstr "Cross-Project Goto Symbol"

#: kateprojectconfigpage.cpp:83
#, kde-format
msgctxt "Groupbox title"
msgid "Git"
msgstr "Git"

#: kateprojectconfigpage.cpp:84
#, kde-format
msgid "Show number of changed lines in git status"
msgstr "Zobraziť počet zmenených riadkov v git status"

#: kateprojectconfigpage.cpp:88
#, kde-format
msgid "Single click action in the git status view"
msgstr "Akcia jedným kliknutím v zobrazení stavu git"

#: kateprojectconfigpage.cpp:90 kateprojectconfigpage.cpp:101
#, kde-format
msgid "No Action"
msgstr "Žiadna akcia"

#: kateprojectconfigpage.cpp:92 kateprojectconfigpage.cpp:103
#, kde-format
msgid "Open file"
msgstr "Otvoriť súbor"

#: kateprojectconfigpage.cpp:93 kateprojectconfigpage.cpp:104
#, kde-format
msgid "Stage / Unstage"
msgstr "Označiť / Zrušiť označenie"

#: kateprojectconfigpage.cpp:99
#, kde-format
msgid "Double click action in the git status view"
msgstr "Akcia dvojitým kliknutím v zobrazení stavu git"

#: kateprojectconfigpage.cpp:135 kateprojectpluginview.cpp:67
#, kde-format
msgid "Projects"
msgstr "Projekty"

#: kateprojectconfigpage.cpp:140
#, kde-format
msgctxt "Groupbox title"
msgid "Projects Properties"
msgstr "Vlastnosti projektu"

#: kateprojectinfoview.cpp:35
#, kde-format
msgid "Terminal (.kateproject)"
msgstr "Terminál (.kateproject)"

#: kateprojectinfoview.cpp:43
#, kde-format
msgid "Terminal (Base)"
msgstr "Terminál (Základný)"

#: kateprojectinfoview.cpp:50
#, kde-format
msgid "Code Index"
msgstr "Index kódu"

#: kateprojectinfoview.cpp:55
#, kde-format
msgid "Code Analysis"
msgstr "Analýza kódu"

#: kateprojectinfoview.cpp:60
#, kde-format
msgid "Notes"
msgstr "Poznámky"

#: kateprojectinfoviewcodeanalysis.cpp:34
#, kde-format
msgid "Start Analysis..."
msgstr "Spustiť analýzu..."

#: kateprojectinfoviewcodeanalysis.cpp:98
#, kde-format
msgid ""
"%1<br/><br/>The tool will be run on all project files which match this list "
"of file extensions:<br/><br/><b>%2</b>"
msgstr ""
"%1<br/><br/>Nástroj bude spustený na všetkých súboroch projektu, ktoré sa "
"zhodujú s týmto zoznamom súborových prípon:<br/><br/><b>%2</b>"

#: kateprojectinfoviewcodeanalysis.cpp:136
#: kateprojectinfoviewcodeanalysis.cpp:196
#: kateprojectinfoviewcodeanalysis.cpp:200
#, kde-format
msgid "CodeAnalysis"
msgstr "Analýza kódu"

#: kateprojectinfoviewcodeanalysis.cpp:191
#, kde-format
msgctxt ""
"Message to the user that analysis finished. %1 is the name of the program "
"that did the analysis, %2 is a number. e.g., [clang-tidy]Analysis on 5 files "
"finished"
msgid "[%1]Analysis on %2 file finished."
msgid_plural "[%1]Analysis on %2 files finished."
msgstr[0] "[%1]Analýza na %2 súbore skončila."
msgstr[1] "[%1]Analýza na %2 súboroch skončila."
msgstr[2] "[%1]Analýza na %2 súboroch skončila."

#: kateprojectinfoviewcodeanalysis.cpp:199
#, kde-format
msgid "Analysis failed with exit code %1, Error: %2"
msgstr "Analýza zlyhala s výstupným kódom %1, Chyba: %2"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Name"
msgstr "Názov"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Kind"
msgstr "Typ"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "File"
msgstr "Súbor"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Line"
msgstr "Riadok"

#: kateprojectinfoviewindex.cpp:34
#, kde-format
msgid "Search"
msgstr "Hľadať"

#: kateprojectinfoviewindex.cpp:195
#, kde-format
msgid "The index could not be created. Please install 'ctags'."
msgstr "Index nie je možné vytvoriť. Prosím nainštalujte 'ctags'."

#: kateprojectinfoviewindex.cpp:205
#, kde-format
msgid "Indexing is not enabled"
msgstr "Indexovanie nie je povolené"

#: kateprojectitem.cpp:166
#, kde-format
msgid "Error"
msgstr "Chyba"

#: kateprojectitem.cpp:166
#, kde-format
msgid "File name already exists"
msgstr "Názov súboru už existuje"

#: kateprojectplugin.cpp:217
#, kde-format
msgid "Confirm project closing: %1"
msgstr "Potvrdiť zatvorenie projektu: %1"

#: kateprojectplugin.cpp:218
#, kde-format
msgid "Do you want to close the project %1 and the related %2 open documents?"
msgstr "Chcete zatvoriť projekt %1 a súvisiace %2 otvorené dokumenty?"

#: kateprojectplugin.cpp:570
#, kde-format
msgid "Full path to current project excluding the file name."
msgstr "Úplná cesta k aktuálnemu projektu bez názvu súboru."

#: kateprojectplugin.cpp:587
#, kde-format
msgid ""
"Full path to current project excluding the file name, with native path "
"separator (backslash on Windows)."
msgstr ""
"Úplná cesta k aktuálnemu projektu s výnimkou názvu súboru, s natívnym "
"oddeľovačom (spätné lomítko vo Windows)."

#: kateprojectplugin.cpp:699 kateprojectpluginview.cpp:73
#: kateprojectpluginview.cpp:225 kateprojectviewtree.cpp:135
#: kateprojectviewtree.cpp:156
#, kde-format
msgid "Project"
msgstr "Projekt"

#: kateprojectpluginview.cpp:57
#, kde-format
msgid "Project Manager"
msgstr "Správca projektu"

#: kateprojectpluginview.cpp:79
#, kde-format
msgid "Open projects list"
msgstr "Otvoriť zoznam projektov"

#: kateprojectpluginview.cpp:84
#, kde-format
msgid "Reload project"
msgstr "Znovu načítať projekt"

#: kateprojectpluginview.cpp:87
#, kde-format
msgid "Close project"
msgstr "Zatvoriť projekt"

#: kateprojectpluginview.cpp:102
#, kde-format
msgid "Refresh git status"
msgstr "Obnoviť stav git"

#: kateprojectpluginview.cpp:180
#, kde-format
msgid "Open Folder..."
msgstr "Otvoriť priečinok..."

#: kateprojectpluginview.cpp:185
#, kde-format
msgid "Project TODOs"
msgstr "Projektové úlohy"

#: kateprojectpluginview.cpp:189
#, kde-format
msgid "Activate Previous Project"
msgstr "Aktivovať predchádzajúci projekt"

#: kateprojectpluginview.cpp:194
#, kde-format
msgid "Activate Next Project"
msgstr "Aktivovať nasledujúci projekt"

#: kateprojectpluginview.cpp:199
#, kde-format
msgid "Lookup"
msgstr "Hľadanie"

#: kateprojectpluginview.cpp:203
#, kde-format
msgid "Close Project"
msgstr "Zatvoriť projekt"

#: kateprojectpluginview.cpp:207
#, kde-format
msgid "Close All Projects"
msgstr "Zatvoriť všetky projekty"

#: kateprojectpluginview.cpp:212
#, kde-format
msgid "Close Orphaned Projects"
msgstr "Zatvoriť opustené projekty"

#: kateprojectpluginview.cpp:222
#, kde-format
msgid "Checkout Git Branch"
msgstr "Získať vetvu git"

#: kateprojectpluginview.cpp:228 kateprojectpluginview.cpp:803
#, kde-format
msgid "Lookup: %1"
msgstr "Hľadanie: %1"

#: kateprojectpluginview.cpp:229 kateprojectpluginview.cpp:804
#, kde-format
msgid "Goto: %1"
msgstr "Prejsť na: %1"

#: kateprojectpluginview.cpp:308
#, kde-format
msgid "Projects Index"
msgstr "Index projektov"

#: kateprojectpluginview.cpp:848
#, kde-format
msgid "Choose a directory"
msgstr "Vyberať adresár"

#: kateprojecttreeviewcontextmenu.cpp:42
#, kde-format
msgid "Enter name:"
msgstr "Zadajte meno:"

#: kateprojecttreeviewcontextmenu.cpp:43
#, kde-format
msgid "Add"
msgstr "Pridať"

#: kateprojecttreeviewcontextmenu.cpp:64
#, kde-format
msgid "Copy Location"
msgstr "Kopírovať umiestnenie"

#: kateprojecttreeviewcontextmenu.cpp:69
#, kde-format
msgid "Add File"
msgstr "Pridať súbor"

#: kateprojecttreeviewcontextmenu.cpp:70
#, kde-format
msgid "Add Folder"
msgstr "Pridať priečinok"

#: kateprojecttreeviewcontextmenu.cpp:77
#, kde-format
msgid "&Rename"
msgstr "Premenovať"

#: kateprojecttreeviewcontextmenu.cpp:84
#, kde-format
msgid "Properties"
msgstr "Vlastnosti"

#: kateprojecttreeviewcontextmenu.cpp:88
#, kde-format
msgid "Open With"
msgstr "Otvoriť s"

#: kateprojecttreeviewcontextmenu.cpp:96
#, kde-format
msgid "Open Internal Terminal Here"
msgstr "Otvoriť vnútorný terminál tu"

#: kateprojecttreeviewcontextmenu.cpp:105
#, kde-format
msgid "Open External Terminal Here"
msgstr "Otvoriť externý terminál tu"

#: kateprojecttreeviewcontextmenu.cpp:110
#, kde-format
msgid "&Open Containing Folder"
msgstr "&Otvoriť priečinok s obsahom"

#: kateprojecttreeviewcontextmenu.cpp:120
#, kde-format
msgid "Show Git History"
msgstr "Zobraziť git históriu"

#: kateprojecttreeviewcontextmenu.cpp:130
#, kde-format
msgid "Delete File"
msgstr "Vymazať súbor"

#: kateprojecttreeviewcontextmenu.cpp:131
#, kde-format
msgid "Do you want to delete the file '%1'?"
msgstr "Chcete vymazať súbor '%1'?"

#: kateprojectviewtree.cpp:135
#, kde-format
msgid "Failed to create file: %1, Error: %2"
msgstr "Nepodarilo sa vytvoriť súbor: %1, Chyba: %2"

#: kateprojectviewtree.cpp:156
#, kde-format
msgid "Failed to create dir: %1"
msgstr "Nepodarilo sa vytvoriť adresár: %1"

#: stashdialog.cpp:37
#, kde-format
msgid "Stash message (optional). Enter to confirm, Esc to leave."
msgstr ""
"Správa úschovne (nepovinné). Enter, ak chcete potvrdiť, Esc, ak chcete odísť."

#: stashdialog.cpp:44
#, kde-format
msgid "Type to filter, Enter to pop stash, Esc to leave."
msgstr ""
"Napíšte, ak chcete filtrovať, Enter, ak chcete aplikovať a vymazať úschovňu, "
"Esc, ak chcete odísť."

#: stashdialog.cpp:143
#, kde-format
msgid "Failed to stash changes %1"
msgstr "Nepodarilo sa uschovať zmeny %1"

#: stashdialog.cpp:145
#, kde-format
msgid "Changes stashed successfully."
msgstr "Zmeny boli úspešne uschované."

#: stashdialog.cpp:164
#, kde-format
msgid "Failed to get stash list. Error: "
msgstr "Nepodarilo sa získať zoznam úschovne Chyba: "

#: stashdialog.cpp:180
#, kde-format
msgid "Failed to apply stash. Error: "
msgstr "Nepodarilo sa aplikovať. Chyba: "

#: stashdialog.cpp:182
#, kde-format
msgid "Failed to drop stash. Error: "
msgstr "Nepodarilo sa vymazať úschovňu. Chyba: "

#: stashdialog.cpp:184
#, kde-format
msgid "Failed to pop stash. Error: "
msgstr "Nepodarilo sa aplikovať a vymazať úschovňu. Chyba: "

#: stashdialog.cpp:188
#, kde-format
msgid "Stash applied successfully."
msgstr "Úschovňa úspešne aplikovaná."

#: stashdialog.cpp:190
#, kde-format
msgid "Stash dropped successfully."
msgstr "Úschovňa úspešne vymazaná."

#: stashdialog.cpp:192
#, kde-format
msgid "Stash popped successfully."
msgstr "Úschovňa úspešne aplikovaná a vymazaná."

#: stashdialog.cpp:221
#, kde-format
msgid "Show stash failed. Error: "
msgstr "Zobrazenie úschovne zlyhalo. Chyba: "

#. i18n: ectx: Menu (projects)
#: ui.rc:9
#, kde-format
msgid "&Projects"
msgstr "Projekty"

#~ msgid "Severity"
#~ msgstr "Závažnosť"

#~ msgid "Message"
#~ msgstr "Správa"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Roman Paholík"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wizzardsk@gmail.com"

#, fuzzy
#~| msgid "Failed to get file history: %1"
#~ msgid "Failed to get file history: git executable not found in PATH"
#~ msgstr "Nepodarilo sa načítať históriu súboru: %1"

#~ msgid "Failed to get file history: %1"
#~ msgstr "Nepodarilo sa načítať históriu súboru: %1"

#, fuzzy
#~| msgid "Show File History"
#~ msgid "Show File Git History"
#~ msgstr "Zobraziť históriu súboru"

#, fuzzy
#~| msgid "Discard"
#~ msgid "Discard Lines"
#~ msgstr "Zahodiť"

#~ msgid "Show diff"
#~ msgstr "Zobraziť rozdiel"

#, fuzzy
#~| msgid "Show Diff"
#~ msgid "Show Raw Diff"
#~ msgstr "Zobraziť rozdiel"

#~ msgid "Copy File Path"
#~ msgstr "Kopírovať cestu k súboru"

#, fuzzy
#~| msgid "Open Folder..."
#~ msgid "Type to filter..."
#~ msgstr "Otvoriť priečinok..."

#~ msgid "Git:"
#~ msgstr "Git:"

#, fuzzy
#~| msgid "<untracked>"
#~ msgid "Untracked (%1)"
#~ msgstr "<nesledované>"

#~ msgid ""
#~ "The index is not enabled. Please add '\"index\": true' to your ."
#~ "kateproject file."
#~ msgstr ""
#~ "Index nie je povolený. Pridajte '\"index\": true' do svojho .kateproject "
#~ "súboru."

#~ msgid "Analysis failed!"
#~ msgstr "Analýza zlyhala!"

#~ msgid "Please install 'cppcheck'."
#~ msgstr "Prosím nainštalujte 'cppcheck'."

#~ msgid "Git Tools"
#~ msgstr "Nástroje Git"

#~ msgid "Launch gitk"
#~ msgstr "Spustiť gitk"

#~ msgid "Launch qgit"
#~ msgstr "Spustiť qgit"

#~ msgid "Launch git-cola"
#~ msgstr "Spustiť git-cola"

#~ msgid "Hello World"
#~ msgstr "Ahoj svet"

#~ msgid "Example kate plugin"
#~ msgstr "Vzorový Kate plugin"
