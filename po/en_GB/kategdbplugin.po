# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2010, 2011.
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 01:00+0000\n"
"PO-Revision-Date: 2023-03-10 17:29+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.70\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "GDB command"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Source file search paths"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Local application"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Remote TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Remote Serial Port"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Host"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Port"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Custom Init Commands"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."

#: configview.cpp:91
#, kde-format
msgid "Add new target"
msgstr "Add new target"

#: configview.cpp:95
#, kde-format
msgid "Copy target"
msgstr "Copy target"

#: configview.cpp:99
#, kde-format
msgid "Delete target"
msgstr "Delete target"

#: configview.cpp:104
#, kde-format
msgid "Executable:"
msgstr "Executable:"

#: configview.cpp:124
#, kde-format
msgid "Working Directory:"
msgstr "Working Directory:"

#: configview.cpp:132
#, kde-format
msgid "Process Id:"
msgstr "Process ID:"

#: configview.cpp:137
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Arguments:"

#: configview.cpp:140
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Keep focus"

#: configview.cpp:141
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Keep the focus on the command line"

#: configview.cpp:143
#, kde-format
msgid "Redirect IO"
msgstr "Redirect IO"

#: configview.cpp:144
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Redirect the debugged programs IO to a separate tab"

#: configview.cpp:146
#, kde-format
msgid "Advanced Settings"
msgstr "Advanced Settings"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Targets"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Target %1"

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Locals"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "CPU registers"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr "Please set the executable in the 'Settings' tab in the 'Debug' panel."

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Could not start debugger process"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb exited normally ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "all threads running"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "thread(s) running: %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "stopped (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Active thread: %1 (all threads stopped)."

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Active thread: %1."

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Current frame: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Host: %1. Target: %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: Could not parse last response: %1"

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "DAP backend failed"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "program terminated"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "requesting disconnection"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "requesting shutdown"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "DAP backend: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Breakpoint(s) reached:"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(continued) thread %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "all threads continued"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(running)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** connection with server closed ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "program exited with code %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** waiting for user actions ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "error on response: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "important"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetry"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "debugging process [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "debugging process %1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Start method: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "thread %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "breakpoint set"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "breakpoint cleared"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) breakpoint"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<not evaluated>"

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "server capabilities"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "supported"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "unsupported"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "conditional breakpoints"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "function breakpoints"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "hit conditional breakpoints"

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "log points"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "modules request"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "goto targets request"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "terminate request"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "terminate debuggee"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "syntax error: expression not found"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "syntax error: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "invalid line: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "file not specified: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "invalid thread id: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "thread id not specified: %1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Available commands:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "conditional breakpoints are not supported by the server"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr "hit conditional breakpoints are not supported by the server"

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "line %1 already has a break point"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "breakpoint not found (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Current thread: "

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "none"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Current frame: "

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Session state: "

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "initialising"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "running"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "stopped"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "terminated"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "disconnected"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "post mortem"

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "command not found"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "missing thread id"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "killing backend"

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Current frame [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Symbol"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Value"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "type"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "indexed items"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "named items"

#: plugin_kategdb.cpp:66
#, kde-format
msgid "Kate GDB"
msgstr "Kate GDB"

#: plugin_kategdb.cpp:70
#, kde-format
msgid "Debug View"
msgstr "Debug View"

#: plugin_kategdb.cpp:70 plugin_kategdb.cpp:285
#, kde-format
msgid "Debug"
msgstr "Debug"

#: plugin_kategdb.cpp:73 plugin_kategdb.cpp:76
#, kde-format
msgid "Locals and Stack"
msgstr "Locals and Stack"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nr"

#: plugin_kategdb.cpp:121
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Frame"

#: plugin_kategdb.cpp:153
#, kde-format
msgctxt "Tab label"
msgid "GDB Output"
msgstr "GDB Output"

#: plugin_kategdb.cpp:154
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Settings"

#: plugin_kategdb.cpp:196
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"

#: plugin_kategdb.cpp:221
#, kde-format
msgid "Start Debugging"
msgstr "Start Debugging"

#: plugin_kategdb.cpp:227
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Kill / Stop Debugging"

#: plugin_kategdb.cpp:233
#, kde-format
msgid "Restart Debugging"
msgstr "Restart Debugging"

#: plugin_kategdb.cpp:239
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Toggle Breakpoint / Break"

#: plugin_kategdb.cpp:245
#, kde-format
msgid "Step In"
msgstr "Step In"

#: plugin_kategdb.cpp:251
#, kde-format
msgid "Step Over"
msgstr "Step Over"

#: plugin_kategdb.cpp:257
#, kde-format
msgid "Step Out"
msgstr "Step Out"

#: plugin_kategdb.cpp:263 plugin_kategdb.cpp:294
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Move PC"

#: plugin_kategdb.cpp:268 plugin_kategdb.cpp:292
#, kde-format
msgid "Run To Cursor"
msgstr "Run To Cursor"

#: plugin_kategdb.cpp:274
#, kde-format
msgid "Continue"
msgstr "Continue"

#: plugin_kategdb.cpp:280
#, kde-format
msgid "Print Value"
msgstr "Print Value"

#: plugin_kategdb.cpp:289
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:373 plugin_kategdb.cpp:389
#, kde-format
msgid "Insert breakpoint"
msgstr "Insert breakpoint"

#: plugin_kategdb.cpp:387
#, kde-format
msgid "Remove breakpoint"
msgstr "Remove breakpoint"

#: plugin_kategdb.cpp:519
#, kde-format
msgid "Execution point"
msgstr "Execution point"

#: plugin_kategdb.cpp:662
#, kde-format
msgid "Thread %1"
msgstr "Thread %1"

#: plugin_kategdb.cpp:762
#, kde-format
msgid "IO"
msgstr "IO"

#: plugin_kategdb.cpp:848
#, kde-format
msgid "Breakpoint"
msgstr "Breakpoint"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Debug"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "GDB Plugin"
msgstr "GDB Plugin"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Andrew Coles, Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "andrew_coles@yahoo.co.uk, steve.allewell@gmail.com"

#~ msgid "GDB Integration"
#~ msgstr "GDB Integration"

#~ msgid "Kate GDB Integration"
#~ msgstr "Kate GDB Integration"

#~ msgid "&Target:"
#~ msgstr "&Target:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "&Arg List:"

#, fuzzy
#~| msgctxt "Program argument list"
#~| msgid "&Arg List:"
#~ msgid "Arg Lists"
#~ msgstr "&Arg List:"

#~ msgid "Add Working Directory"
#~ msgstr "Add Working Directory"

#~ msgid "Remove Working Directory"
#~ msgstr "Remove Working Directory"
